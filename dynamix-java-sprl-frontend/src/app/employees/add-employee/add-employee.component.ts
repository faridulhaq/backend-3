import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MenuItem } from 'primeng';
import { EmployeeService } from '../employee.service';
import { ActivatedRoute, Router } from '@angular/router';
import {ConfirmationDialogComponent} from "../confirmation-dialog/confirmation-dialog.component";
import { MatDialog } from '@angular/material/dialog';
import { Employee } from '../employee';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  
 
  employeeId: number;
  employee: Employee;
  employeeForm : FormGroup;
  submitted: boolean;
  actionItems : MenuItem[];
  photoUpload : any[] = [];
  
  constructor(private route:ActivatedRoute, private router: Router,private fb: FormBuilder, public dialog: MatDialog, private employeeService: EmployeeService) {
    this.actionItems = [
      {
        label : 'Delete', icon: 'pi pi-times', command : () => this.delete(this.employee)
      }      
    ];
    
    this.newEmployee();
    
    this.employeeId = this.route.snapshot.params['employeeId'];
    this.employeeService.getEmployee(this.employeeId)
    .subscribe(data => {
      console.log(data)
      this.employee = data;
    }, error => console.log(error));

  }


  ngOnInit(): void {
    this.employeeForm = this.fb.group({
      'firstName':new FormControl('',Validators.required),
      'lastName':new FormControl('',Validators.required),
      'email':new FormControl('',[Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$") ]),
      'phone':new FormControl('',[ Validators.required,Validators.pattern('^[0-9]*$') ]),
      'userName':new FormControl('', Validators.required),
      'password':new FormControl('', Validators.required)

    });

    if(this.employee != null) this.employeeForm.patchValue(this.employee);
  }

  get diagnostic() { return JSON.stringify(this.employeeForm.value); }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  delete(employee:Employee) {
    this.employeeId = employee.employeeId;
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: {id : employee.employeeId, firstName :employee.firstName, lastName :employee.lastName}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  newEmployee(): void {
    this.submitted = false;
    this.employee = new Employee();
  }

  save() {
    console.log(this.employee);
    this.employeeService.createEmployee(this.employee)
      .subscribe(data => console.log(data), error => console.log(error));
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/employees']);
  }

  onReset() {
    this.submitted = false;
    this.employeeForm.reset();
  }
}
