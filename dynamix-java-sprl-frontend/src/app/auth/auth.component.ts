import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { AuthenticationService } from '../core/services/authentication.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  loginForm: FormGroup;
  invalidLogin: boolean = false;

  constructor(
    private messageService: MessageService,
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService) { }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }

    const uName = this.loginForm.controls.username.value;
    const pswd = this.loginForm.controls.password.value;
    this.authenticationService.authenticate(uName, pswd).subscribe(data => {
      this.messageService.add({ severity: 'success', summary: 'Success Message', detail: 'Login Successful' });
      this.authenticationService.setAuthentication(true);
      // Redirect to home screen
      this.router.navigate(['employees']);
    }, error => {
      this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Authentication Failed' });
    });
  }

  ngOnInit() {
    if (this.authenticationService.isUserLoggedIn()) {
      this.authenticationService.setAuthentication(true);
      this.router.navigate(['employees']);
      return;
    }
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.required]
    });
  }

}
