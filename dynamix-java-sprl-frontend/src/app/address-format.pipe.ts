import { Pipe, PipeTransform } from '@angular/core';
import { Address } from './address';

@Pipe({ name: 'addressFormat' })
export class AddressFormat implements PipeTransform{
    transform ( address: Address ): string {
        let fromattedAddress = 
            address.address1 + ", " + 
            address.address2 + ", " +
            address.city     + ", " +
            address.country;

        if( address.postalCode ) {
            fromattedAddress += ", " +  address.postalCode
        }

        return fromattedAddress;
    }
}