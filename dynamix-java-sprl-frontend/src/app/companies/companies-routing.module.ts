import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompaniesComponent } from './companies.component';
import { CompanyViewComponent } from './company-view/company-view.component';
import { CompanyAddComponent } from './company-add/company-add.component';
import { CompanyResolverService } from './company-resolver.service';

const routes: Routes = [
  { path: '', component: CompaniesComponent },
  { path: ':companyId', component: CompanyViewComponent,
          resolve: { resolvedData: CompanyResolverService } },
  { path: 'add', component: CompanyAddComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompaniesRoutingModule { }
