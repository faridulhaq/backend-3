
export class Address {
    addressId: number;
    address1?: string;
    address2?: string;
    city?: string;
    country?: string;
    postalCode: number;
}