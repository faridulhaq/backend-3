import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConsultantService } from './consultant.service';
import { Consultant } from '../models/consultant.interface';
import { SelectItem } from 'primeng/api/selectitem';

@Component({
  selector: 'app-consultants',
  templateUrl: './consultants.component.html',
  styleUrls: ['./consultants.component.css']
})
export class ConsultantsComponent implements OnInit {

  consultants : Consultant[];

  sortOptions: SelectItem[];

  sortKey: string;

  sortField: string;

  sortOrder: number;

  availabilityDateRange: Date[];

  constructor( private consultantService: ConsultantService, private router: Router ) { }

  ngOnInit(): void {
    this.consultantService.getConsultants().subscribe(consultants => this.consultants = consultants.data);

    this.sortOptions = [
      {label:'Name',value:'firstName'},
      {label:'Availability',value:'availabilityDate'}
    ]
  }

  selectConsultant(event: Event, consultantId: number) {
    this.router.navigate(['consultants/consultant',consultantId]);
    event.preventDefault();
}

onSortChange(event) {
    let value = event.value;

    if (value.indexOf('!') === 0) {
        this.sortOrder = -1;
        this.sortField = value.substring(1, value.length);
    }
    else {
        this.sortOrder = 1;
        this.sortField = value;
    }
}

  addConsultant(event){
    this.router.navigate(['consultants/consultant',0]);
  }

}