INSERT INTO `users_database`.`role` (`roleId`, `typeRole`) VALUES (1, 'HR');
INSERT INTO `users_database`.`role` (`roleId`, `typeRole`) VALUES (2, 'Manager');
INSERT INTO `users_database`.`employee` (`employeeId`, `emailAddress`, `firstName`, `lastName`, `phone`) VALUES ('1', 'Administrator@admin.com', 'admin', 'admin', '1111');
INSERT INTO `users_database`.`user` (`userId`, `login`, `password`, `employeeId`, `roleId`) VALUES ('1', 'admin', '$2a$04$mQZ.Hvefo5o577.p90IFEO/WwbaSVRPYqCRo1JjQ.8hTNZE.fWhmq', '1', '2');
