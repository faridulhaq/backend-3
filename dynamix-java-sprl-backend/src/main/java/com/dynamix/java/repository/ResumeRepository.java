package com.dynamix.java.repository;

import com.dynamix.java.model.Resume;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface ResumeRepository extends JpaRepository<Resume, Long> {

    Optional<List<Resume>> findByReceptionDate(LocalDate date);

    Optional<Resume> findByConsultant_ConsultantId(Long id);

}
