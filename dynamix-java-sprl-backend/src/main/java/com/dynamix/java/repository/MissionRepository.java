package com.dynamix.java.repository;

import com.dynamix.java.model.Mission;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MissionRepository extends JpaRepository<Mission, Long>{
	
	List<Mission> findByContract_id(Long contractId);

}