package com.dynamix.java.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

public enum MaritalStatus {

    SINGLE(Values.SINGLE), MARRIED(Values.MARRIED), DIVORCED(Values.DIVORCED), WIDOWER(Values.WIDOWER), WIDOWED(Values.WIDOWED);

    private String value;

    MaritalStatus(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static class Values {
        public static final String SINGLE = "Single";
        public static final String MARRIED = "Married";
        public static final String DIVORCED = "Divorced";
        public static final String WIDOWER = "Widower";
        public static final String WIDOWED = "Widowed";
    }

    @Converter(autoApply = true)
    public static class AttibuteValueConverter implements AttributeConverter<MaritalStatus, String> {

        @Override
        public String convertToDatabaseColumn(MaritalStatus maritalStatus) {
            if (maritalStatus == null) {
                return null;
            }
            return maritalStatus.getValue();
        }

        @Override
        public MaritalStatus convertToEntityAttribute(String value) {
            if (value == null) {
                return null;
            }
            return Stream.of(MaritalStatus.values())
                    .filter(c -> c.getValue().equals(value))
                    .findFirst()
                    .orElseThrow(IllegalArgumentException::new);
        }
    }
}