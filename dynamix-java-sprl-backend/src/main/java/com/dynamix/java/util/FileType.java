package com.dynamix.java.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

public enum FileType {
	PDF(Values.PDF),MSWORD(Values.MSWORD);
	
	private FileType(final String value){
		this.value=value;
	}
	
	private String value;
	
	public String getValue() {
		return value;
	}
	
	public static FileType fromValue(String value) {
		switch(value) {
		case Values.PDF:
			return FileType.PDF;
		case Values.MSWORD:
			return FileType.MSWORD;
		default:
			throw new IllegalArgumentException("Value [" +value+" ] not supported.");
		}
	}
	
	@Override
	public String toString() {
		return this.value;
	}
	
	public static class Values{
		public static final String PDF = "Pdf";
		public static final String MSWORD = "MS Word";
	}
	
	@Converter(autoApply=true)
	public static class FileTypeConverter implements AttributeConverter<FileType,String>{

		@Override
		public String convertToDatabaseColumn(FileType attribute) {
			return attribute == null ? null : attribute.getValue();
		}

		@Override
		public FileType convertToEntityAttribute(String dbData) {
			return dbData == null ? null : FileType.fromValue(dbData);
		}
	}
}
