package com.dynamix.java.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

public enum Title {

    DEVELOPER(Values.DEVELOPER), SCRUMMASTER(Values.SCRUMMASTER), ARCHITECT(Values.ARCHITECT);

    private String value;

    Title(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static class Values {
        public static final String DEVELOPER = "Developer";
        public static final String SCRUMMASTER = "ScrumMaster";
        public static final String ARCHITECT = "Architect";
    }

    @Converter(autoApply = true)
    public static class AttibuteValueConverter implements AttributeConverter<Title, String> {

        @Override
        public String convertToDatabaseColumn(Title title) {
            if (title == null) {
                return null;
            }
            return title.getValue();
        }

        @Override
        public Title convertToEntityAttribute(String value) {
            if (value == null) {
                return null;
            }
            return Stream.of(Title.values())
                    .filter(c -> c.getValue().equals(value))
                    .findFirst()
                    .orElseThrow(IllegalArgumentException::new);
        }
    }
}