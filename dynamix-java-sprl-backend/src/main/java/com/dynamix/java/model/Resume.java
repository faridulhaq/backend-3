package com.dynamix.java.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "resume")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Resume {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long resumeId;

    @Column(name = "receptiondate", nullable = false)
    @JsonFormat(pattern = "MM/dd/yyyy")
    private LocalDate receptionDate;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "resume", fetch = FetchType.LAZY)
    private Set<File> files = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "resumeTechId.resume")
    private Set<ResumeTech> resumeTechs = new HashSet<>();

    @OneToOne(mappedBy = "resume")
    @JsonIgnore
    private Consultant consultant;

    public Resume() {
    }

    public long getResumeId() {
        return resumeId;
    }

    public void setResumeId(long resumeId) {
        this.resumeId = resumeId;
    }

    public LocalDate getReceptionDate() {
        return receptionDate;
    }

    public void setReceptionDate(LocalDate receptionDate) {
        this.receptionDate = receptionDate;
    }

    public Set<File> getFiles() {
        return files;
    }

    public void setFiles(Set<File> files) {
        this.files = files;
        for (File file : files) {
            file.setResume(this);
        }
    }

    public Set<ResumeTech> getResumeTechs() {
        return resumeTechs;
    }

    public void setResumeTechs(Set<ResumeTech> resumeTechs) {
        this.resumeTechs = resumeTechs;
        for (ResumeTech resumeTech : resumeTechs) {
            resumeTech.getResumeTechId().setResume(this);
        }
    }

    public Consultant getConsultant() {
        return consultant;
    }

    public void setConsultant(Consultant consultant) {
        this.consultant = consultant;
    }
}
