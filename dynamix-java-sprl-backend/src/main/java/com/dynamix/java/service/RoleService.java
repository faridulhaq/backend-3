package com.dynamix.java.service;

import java.util.List;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Role;


public interface RoleService {
	
	List<Role> getAllRoles();
	Role getRoleById(Long roleId) throws ResourceNotFoundException;
}
