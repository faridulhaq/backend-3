package com.dynamix.java.service;

import java.time.LocalDate;
import java.util.List;
import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Timesheet;


public interface TimesheetService {
	
	List<Timesheet> getAllTimesheets();
	Timesheet getTimesheetByMissionIdAndMonth(Long missionId,LocalDate month) ;
	List<Timesheet> getTimesheetByMissionId(Long missionId) ;
	Timesheet createTimesheet(Timesheet timesheet);
	Timesheet updateTimesheet(Long timesheetId, Timesheet timesheetDetails) throws ResourceNotFoundException;
	void deleteTimesheet(Long timesheetId) throws ResourceNotFoundException;

}
