package com.dynamix.java.service;

import java.util.List;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.File;


public interface FileService {
	
	List<File> getAllFiles();
	List<File> getFilesByResumeId(Long resumeId) ;
	File createFile(File file);
	File updateFile(Long fileId, File fileDetails) throws ResourceNotFoundException;
	void deleteFile(Long fileId) throws ResourceNotFoundException;

}
