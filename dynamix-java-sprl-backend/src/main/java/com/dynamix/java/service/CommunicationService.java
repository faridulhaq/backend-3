package com.dynamix.java.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Communication;
import com.dynamix.java.repository.CommunicationRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommunicationService {

    @Autowired
    private CommunicationRepository communicationRepository;

    public List<Communication> getAllCommunications() {
		return communicationRepository.findAll();
	}

	public Optional<Communication> getCommunicationById(Long communicationId) {
		return communicationRepository.findById(communicationId);
	}

	public List<Communication> getCommunicationByEmployeeId(Long employeeId) {
		return communicationRepository.findByEmployeeEmployeeId(employeeId);
	}

	public List<Communication> getCommunicationByContactId(Long contactId) {
		return communicationRepository.findByContactContactId(contactId);
	}

	public Communication createUpdateCommunication(@Valid Communication Communication) {
        return communicationRepository.save(Communication);
	}
	
	public Map<String, Boolean> deleteCommunication(Long communicationId) throws ResourceNotFoundException {
        Communication Communication = communicationRepository.findById(communicationId)
				.orElseThrow(() -> new ResourceNotFoundException("Communication not found for this id :: " + communicationId));
                communicationRepository.delete(Communication);
        
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}