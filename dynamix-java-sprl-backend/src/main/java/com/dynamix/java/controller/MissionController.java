package com.dynamix.java.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Mission;
import com.dynamix.java.service.MissionService;
import com.dynamix.java.util.ReasonEnd;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1/missions")
public class MissionController {

    @Autowired
    private MissionService missionService;

    @GetMapping
	public List<Mission> getAllMissions() {
		return missionService.getAllMissions();
    }
    
	@GetMapping("/{id}")
	public ResponseEntity<Mission> getMissionById(@PathVariable(value = "id") Long missionId)
			throws ResourceNotFoundException {
		Mission mission = missionService.getMissionById(missionId)
				.orElseThrow(() -> new ResourceNotFoundException("Mission not found for this id :: " + missionId));
		return ResponseEntity.ok().body(mission);
	}
    
	@GetMapping("/contract/{contractId}")
	public ResponseEntity<List<Mission>> getMissionByContractId(@PathVariable(value = "contractId") Long contractId)
			throws ResourceNotFoundException {
		List<Mission> missions = missionService.getMissionByContractId(contractId);
		return ResponseEntity.ok().body(missions);
	}

	@PostMapping("/{contractId}")
	public Mission createMission(@PathVariable(value = "contractId") Long contractId,
			@Valid @RequestBody Mission mission) throws ResourceNotFoundException {
		return missionService.createUpdateMission(contractId, mission);
	}
	
	@GetMapping("/endReason")
	public ResponseEntity<ReasonEnd[]> getMissionEndReasons() throws ResourceNotFoundException {
		return ResponseEntity.ok()
				.body(ReasonEnd.values());
	}

	@PutMapping("/{id}/{contractId}")
	public ResponseEntity<Mission> updateMission(@PathVariable(value = "id") Long missionId,
			@PathVariable(value = "contractId") Long contractId, @Valid @RequestBody Mission missionDetails)
			throws ResourceNotFoundException {

		Mission mission = missionService.getMissionById(missionId)
				.orElseThrow(() -> new ResourceNotFoundException("Mission not found for this id :: " + missionId));

		mission.setStartDate(missionDetails.getStartDate());
		mission.setEndDate(missionDetails.getEndDate());
		mission.setRate(missionDetails.getRate());
		mission.setLocationCity(missionDetails.getLocationCity());
		mission.setReasonEnd(missionDetails.getReasonEnd());
		mission.setDescription(missionDetails.getDescription());
		mission.setEvaluation(missionDetails.getEvaluation());
		final Mission updatedMission = missionService.createUpdateMission(contractId, mission);
		return ResponseEntity.ok(updatedMission);
	}
    
    @DeleteMapping("/missions/{id}")
	public Map<String, Boolean> deleteMission(@PathVariable(value = "id") Long missionId)
			throws ResourceNotFoundException {
		return missionService.deleteMission(missionId);
	}
    
}