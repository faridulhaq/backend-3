package com.dynamix.java.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Company;
import com.dynamix.java.service.CompanyService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class CompanyController {
	@Autowired
	private CompanyService companyService;
	
	@GetMapping("/companies")
	public List<Company> getAllCompanys() {
		return companyService.getAllCompanies();
	}

	@GetMapping("/company/{id}")
	public ResponseEntity<Company> getCompanyById(@PathVariable(value = "id") Long companyId)
			throws ResourceNotFoundException {
		Company company = companyService.getCompanyById(companyId);
		return ResponseEntity.ok().body(company);
	}

	@PostMapping("/company")
	public Company createCompany(@Valid @RequestBody Company company) {
		return companyService.createCompany(company);
	}

	@PutMapping("/company/{id}")
	public ResponseEntity<Company> updateCompany(@PathVariable(value = "id") Long companyId,
			@Valid @RequestBody Company companyDetails) throws ResourceNotFoundException {
		Company updatedCompany = companyService.updateCompany(companyId, companyDetails);
		return ResponseEntity.ok(updatedCompany);
	}

	@DeleteMapping("/company/{id}")
	public Map<String, Boolean> deleteCompany(@PathVariable(value = "id") Long companyId)
			throws ResourceNotFoundException {
		companyService.deleteCompany(companyId);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
