package com.dynamix.java.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Role;
import com.dynamix.java.service.RoleService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class RoleController {
	@Autowired
	private RoleService roleService;
	
	@GetMapping("/roles")
	public List<Role> getAllRoles() {
		return roleService.getAllRoles();
	}

	@GetMapping("/role/{id}")
	public ResponseEntity<Role> getRoleById(@PathVariable(value = "id") Long roleId)
			throws ResourceNotFoundException {
		Role role = roleService.getRoleById(roleId);
		return ResponseEntity.ok().body(role);
	}

}
